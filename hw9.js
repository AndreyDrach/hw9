// Теоретичні питання
// Опишіть, як можна створити новий HTML тег на сторінці.
// document.createElement(tagName)

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Перший параметр - позиція вставки: beforebegin - перед елементом; afterbegin - всередині елемента, на початку;
// beforeend - всередині елемента, в кінці; afterend - після елементу.

// Як можна видалити елемент зі сторінки?
// Element.remove()


const transforArray = (arr) => {
  const listArr = arr.map((item) => `<li> ${item}</li>`).join("");
  const plase = document.querySelector("body");
  plase.insertAdjacentHTML("beforeend", `<ul>${listArr}</ul>`);
};


transforArray(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
transforArray(["1", "2", "3", "sea", "user", 23]);